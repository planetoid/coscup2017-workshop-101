## Ansible & GitLab CI/CD workshop 101

[![build status](https://gitlab.com/DevOpsTaiwan/coscup2017-workshop-101/badges/master/build.svg)](https://gitlab.com/DevOpsTaiwan/coscup2017-workshop-101/commits/master) [![coverage report](https://gitlab.com/planetoid/coscup2017-workshop-101/badges/master/coverage.svg)](https://gitlab.com/planetoid/coscup2017-workshop-101/commits/master)

此範例程式碼即是 [PHP Framework Laravel 5.4.*](https://laravel.com/docs/5.4#installation)，如果你熟悉 Laravel，你也可以自行預備。

取得程式碼後，即可依據 Workshop 101 的步驟開始嘗試建立你自己的 CI/CD Pipeline。

## 操作步驟

[艦長，你有事嗎？: COSCUP 2017 \- Ansible & GitLab CI/CD workshop 101](http://blog.chengweichen.com/2017/08/coscup-2017-ansible-gitlab-cicd.html)

* 啟動 docker
  * 原 `docker run -d -i --name web-server1 chengweisdocker/coscup2017:webserver` 修改為 `sudo docker run -d -i --name web-server1 -p 0.0.0.0:80:80 chengweisdocker/coscup2017:webserver` 其中 `-p 80:80` 代表將連到 docker host 的 port 80 的連線轉至 container 的 port 80 ref: [\[Docker\] 設定 container 裡的固定 IP address \| EPH 的程式日記](https://ephrain.net/docker-%E8%A8%AD%E5%AE%9A-container-%E8%A3%A1%E7%9A%84%E5%9B%BA%E5%AE%9A-ip-address/)
  * 在 Settings > CI / CD Settings > Variables: `stg_domain_name` 與 `stg_host_ip` 設定是外部 IP
  * stage: stg-deploy 通過後，可以瀏覽器連線外部 IP

下面多寫了新建出來的 IP，就是只有連到這 IP 的 port 80 時才會導到 container 去：
* 在 Settings > CI / CD Settings > Jobs 直接顯示 coverage (1) Settings > CI / CD Settings > General pipelines > Test coverage parsing (2) phpunit 需要設定 `--colors=never`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
